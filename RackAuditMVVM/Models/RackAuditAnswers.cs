﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RackAuditMVVM.Models
{
    /// <summary>
    /// Represent any Answer, regardless of its type.
    /// </summary>
    public class RackAuditAnswers
    {
        
        public int myKey { get; set; }
        public int questionKey { get; set; }
        public string answer { get; set; }
        public DateTime myTime { get; set; }
        public int type { get; set; }
        /// <summary>
        /// default constructor need only for seralization
        /// </summary>
        public RackAuditAnswers()
        {
        }
        public RackAuditAnswers(RackAuditQuestions q)
        {
            this.questionKey = q.myKey;  
            this.myTime = DateTime.Now;
            this.type = q.type;
            if (q.type == 0)
            {
                if (q.Pressed == true)
                    this.answer = "Yes";
                else
                    this.answer = "No";
            }
            else if(q.type == 3)
            {
                if (q.Pressed  == true)
                    this.answer = "Pass";
                else
                    this.answer = "Fail";
            }
            else
            {
                this.answer = q.Answer;
            }
        }
    }


}
