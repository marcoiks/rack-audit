﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RackAuditMVVM.Models
{
    /// <summary>
    /// Represents a Ticket and some of the model data useful for this program
    /// </summary>
    public class FinishedGoods
    {
        public int ticket { get; set; }
        public int customer;
        public string po;
        public string line;
        public string status;
        public string marking;
        public string sizex { get; set; }
        public string sizey { get; set; }
        public int schedule { get; set; }
        public int job { get; set; }
        public DateTime timeProduced { get; set; }
        public DateTime timeShipped { get; set; }
        public DateTime timeScheduled { get; set; }
        public float thickness { get; set; }
        public string treatment { get; set; }
        public int shift { get; set; }
        public string crew { get; set; }
        public string rackType { get; set; }
        public string rackCode { get; set; }
        public float tons { get; set; }
        public float sqft { get; set; }
        public int litesPerRack { get; set; }
        public string area { get; set; }
        public int whzone { get; set; }
        public string whrow { get; set; }
        public int shippingNumber { get; set; }
        public int plant { get; set; }
        public int myKey { get; set; }
        public int racks { get; set; }
        /*public DateTime productionDate1 { get; set; }
        public DateTime productionDate2 { get; set; }
        public int productionShift1 { get; set; }
        public int productionShift2 { get; set; }
        public string productionCrew1 { get; set; }
        public string productionCrew2 { get; set; }
        public int productionLites1 { get; set; }
        public int productionLites2 { get; set; }
        public int releaseStatus { get; set; }
        public int solarShippedStatus { get; set; }
        public int rack { get; set; }
        public int rackID { get; set; }
        public string packLocation { get; set; }
        public int inspectedBy { get; set; }
        public int closingShift { get; set; }
        public decimal glassWeight { get; set; }
        public decimal grossWeight { get; set; }
        public string E1Part { get; set; }
        public int targetLites { get; set; }
        public string targetRackType { get; set; }
        public string overPack { get; set; }
        public string underPack { get; set; }
        public string thicknessSpec { get; set; }
        public DateTime packStart { get; set; }
        public DateTime packEnd { get; set; }
        public string packedBy { get; set; }
        public string inspInitials { get; set; }
        public int processingStatus { get; set; }
        public int ticketStyle { get; set; }
        public string customerPart { get; set; }
        public string closingComments { get; set; }
        public string longItem { get; set; }
        public int workOrder { get; set; }
        public string workOrderType { get; set; }
        public string ticketComment1 { get; set; }
        public string ticketComment2 { get; set; }
        public int holdCode { get; set; }
        public string customerName { get; set; }
        public string ticketFile { get; set; }
        public int sourceTicket1 { get; set; }
        public int sourceTicket2 { get; set; }
        public int sourceTicket3 { get; set; }
        public int curPlant { get; set; }
        public string curStatus { get; set; }
        public int curLites { get; set; }
        public DateTime consumedTime { get; set; }
        public DateTime consumedDate1 { get; set; }
        public DateTime consumedDate2 { get; set; }
        public int consumedShift1 { get; set; }
        public int consumedShift2 { get; set; }
        public string consumedCrew1 { get; set; }
        public string consumedCrew2 { get; set; }
        public int consumedLites1 { get; set; }
        public int consumedLites2 { get; set; }
        public int consumedArea { get; set; }
        public int consumedLine { get; set; }
        public string whLoc { get; set; }
        /* DateTime lastUpdate;
         //    [upload] [bit] NULL,
         //[transferred] [bit] NULL,
         //[transferredDateTime] [datetime] NULL,
         //[comment] [varchar](1024) NULL,
         //[logo] [varchar](5) NULL,
         //[measurementInches] [float] NULL,
         //[consumedWorkOrder1] [int] NULL,
         //[consumedWorkOrder2] [int] NULL,
         //[poReferenceNumber] [varchar](20) NULL,
         //[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [MSmerge_df_rowguid_E963B2DD881D4334A21DF24A8F8E7B8F]  DEFAULT (newsequentialid()),
         //[taggedBy] [varchar](10) NULL,
         //[tonsPerLite] [float] NULL,
         //[consumedArea1] [varchar](2) NULL,
         //[consumedArea2] [varchar](2) NULL,
         //[plannerNumber] [int] NULL,
         //[salesOrder] [int] NULL,
         //[salesOrderType] [char](2) NULL,
         //[salesOrderLine] [decimal](18, 3) NULL,
         //[consumedLine2] [int] NULL,
         //[coating1] [varchar](10) NULL,
         //[coating2] [varchar](10) NULL,
         //[pattern1] [varchar](10) NULL,
         //[pattern2] [varchar](10) NULL,
         //[color] [varchar](10) NULL,
         //[drawing] [varchar](20) NULL,
         //[transmittance] [float] NULL,
         //[longCustomerPO] [varchar](20) NULL,
         //[partialReason] [int] NULL,
         //[voidReason] [int] NULL,
         //[voidTime] [datetime] NULL,
         //[e1ThicknessName] [varchar](10) NULL,
         //[setupInitials] [varchar](10) NULL,
         //[setupTime] [datetime] NULL,
         //[setupCrew] [varchar](3) NULL
         * */
    }

    public class FinishedGoodsCollection : ObservableCollection<FinishedGoods>
    {
        public void CopyFrom(IEnumerable<FinishedGoods> fg)
        {
            this.Items.Clear();
            foreach (var p in fg)
            {
                this.Items.Add(p);
            }

            this.OnCollectionChanged(
                new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}
