﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace RackAuditMVVM.Models
{
    /// <summary>
    /// Represents a Question for all types
    /// and handles the Inotify protocol for binding
    /// </summary>
    public class RackAuditQuestions : INotifyPropertyChanged
    {
        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public int myKey { get; set; }
        public string setupCode { get; set; }
        public string closingCode { get; set; }
        public string question { get; set; }
        public int type { get; set; }
   
        private string _answer; 
        public string Answer
        {
            get
            {
                return _answer;
            }
            set
            {
                int temp;
                if (type == 4)
                {

                    bool isNumeric = int.TryParse(value, out temp);
                    if(isNumeric)
                    {
                        _answer = value;
                    }
                }
                else
                {
                    _answer = value;
                }
                RaisePropertyChanged();
            }

        }

        private bool? _pressed;
        public bool? Pressed
        {
            get { return _pressed; }
            set
            {
                _pressed = value;
                if (type == 0)
                {
                    if (value == true)
                    {
                        Answer = "Yes";
                        PushColor = "Green";
                    }

                    else if( value == false)
                    {
                        Answer = "No";
                        PushColor = "Red";
                    }
                    else
                    {
                        Answer = "";                        
                    }

                }
                else if(type == 3)
                {
                    if (value == true)
                    {
                        Answer = "Pass";
                        PushColor = "Green";
                    }
                    else if(value == false)
                    {
                        Answer = "Fail";
                        PushColor = "Red";
                    }
                }
                else if(type == 4)
                {
                    Answer = "";
                    
                }
                else
                    Answer = "";
                

                RaisePropertyChanged();
            }
        }
        private string _pushColor;
        public string  PushColor
        {
            get { return _pushColor; }
            set
            {
                if(_pushColor != value)
                {
                    _pushColor = value;
                    RaisePropertyChanged();
                }
            }
        }

        private BitmapImage _image;
        public BitmapImage Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged();
            }
        }

        public IRandomAccessStream pictureStream
        {
            get;
            set;
        }

        public RackAuditQuestions()
        {
            Pressed = null;
        }

    }

}
