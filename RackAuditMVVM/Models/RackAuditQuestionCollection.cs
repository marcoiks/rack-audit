﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RackAuditMVVM.Models
{
    /// <summary>
    /// Observable collection of Questions
    /// </summary>
    public class RackAuditQuestionsCollection : ObservableCollection<RackAuditQuestions>
    {
        public void CopyFrom(IEnumerable<RackAuditQuestions> fg)
        {
            this.Items.Clear();
            foreach (var p in fg)
            {
                p.Pressed = false;                
                this.Items.Add(p);
            }

            this.OnCollectionChanged(
                new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        public void Reset()
        {
            foreach (var i in this.Items)
            {
                if (i.pictureStream != null)
                    i.pictureStream.Dispose();
            }
            this.Items.Clear();
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}
