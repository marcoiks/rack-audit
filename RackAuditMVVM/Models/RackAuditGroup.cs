﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RackAuditMVVM.Models
{
    /// <summary>
    /// Rack Audit Group represents a set of Questions
    /// </summary>
    public class RackAuditGroup
    {
        private List<RackAuditAnswers> _items = new List<RackAuditAnswers>();
        public List<RackAuditAnswers> Items { get { return _items; } }
        public int ticket { get; set; }
        public string initials { get; set; }
        public int submitPlant { get; set; }
        public int questionGroupID { get; set; }
        public RackAuditGroup()
        {

        }

        public void Add(RackAuditAnswers item)
        {
            _items.Add(item);
        }
    }
}
