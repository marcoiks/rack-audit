﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RackAuditMVVM.Models
{
    /// <summary>
    /// Represents a Question Group
    /// </summary>
    public class RackAuditQuestionGroup
    {
        public int myKey { get; set; }
        public string name { get; set; }
        public int type { get; set; }
        public string description { get; set; }
        public string setupCode { get; set; }
        public string closingCode { get; set; }
        public int plant { get; set; }        
        public RackAuditQuestionGroup()
        {

        }
    }
}
