﻿using RackAuditMVVM.Commands;
using RackAuditMVVM.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Imaging;
using ZXing;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Networking.Connectivity;
using System.Linq;
using System.Collections.ObjectModel;
using RackAuditMVVM.Common;

namespace RackAuditMVVM.ViewModel
{
    /// <summary>
    /// View Model for the Rack Audit, handles most of the policy and talks to the WebApi
    /// </summary>
    public class RackAuditVM : CommonBaseVM
    {
        #region Constants

        private const int TICKETLENGTH = 10;
        private const int TICKETFACTOR = 10000000;
        #endregion
        //API root server
        private string _apiRoot { get; set; }
        //Primary list of questions 
        private RackAuditQuestionsCollection _questionList;
        public RackAuditQuestionsCollection QuestionList
        {
            get
            {
                return _questionList;
            }
            set
            {
                _questionList = value;
            }
        }

        ApplicationDataContainer localSettings;
        HttpClient _client = new HttpClient();
        private FinishedGoods _currentTicket;
        protected readonly CoreDispatcher _dispatcher;
        private bool submitEnabled = true;
        private int _currentPlant;
        public int _presetAuditKey { get; set; }

        #region ViewProperties
        private ObservableCollection<RackAuditQuestionGroup> _questionsGroup;
        public ObservableCollection<RackAuditQuestionGroup> QuestionGroup
        {
            get
            {
                return _questionsGroup;
            }
            set
            {
                _questionsGroup = value;

                RaisePropertyChanged();
            }
        }

        private RackAuditQuestionGroup _currentAudit;
        public RackAuditQuestionGroup CurrentAudit
        {
            get { return _currentAudit; }
            set
            {
                _currentAudit = value;
                _presetAuditKey = _currentAudit.myKey;
                localSettings.Values["selectedQuestionGroup"] = _currentAudit.myKey;
                RaisePropertyChanged();
                (SearchTicketCommand as DelegateCommand).RaiseCanExecuteChanged();
            }
        }

        public FinishedGoods CurrentTicket
        {
            get
            {
                return _currentTicket;
            }
            set
            {
                _currentTicket = value;
                RaisePropertyChanged();
                RaisePropertyChanged("TicketSize");
            }
        }
        public string TicketSize
        {
            get
            {
                if (_currentTicket != null)
                    return _currentTicket.sizex + "/" + _currentTicket.sizey;
                else
                    return "----";
            }
        }
        private string _ticketSerial;
        public string TicketSerial
        {
            get
            {
                return _ticketSerial;
            }
            set
            {
                if (value != _ticketSerial)
                {
                    _ticketSerial = value;
                    RaisePropertyChanged();
                    (SearchTicketCommand as DelegateCommand).RaiseCanExecuteChanged();
                    (SubmitInspectionCommand as DelegateCommand).RaiseCanExecuteChanged();
                }
            }
        }


        private string _initials;
        public string Initials
        {
            get { return _initials; }
            set
            {
                if (_initials != value)
                {
                    _initials = value;
                    RaisePropertyChanged();
                    if (_initials.Trim() == "")
                    {
                        SubmitText = "Enter Initials to Submit";
                    }
                    else
                    {
                        SubmitText = "Submit";
                    }

                    (SubmitInspectionCommand as DelegateCommand).RaiseCanExecuteChanged();
                }
            }
        }

        private string _submitText = "Enter Initials to Submit";
        public string SubmitText
        {
            get { return _submitText; }
            set
            {
                if (_submitText != value)
                {
                    _submitText = value;
                    RaisePropertyChanged();

                }
            }
        }

        #endregion

        #region Commands

        public ICommand NewInspectionCommand { get; private set; }
        public ICommand SearchTicketCommand { get; private set; }
        public ICommand SubmitInspectionCommand { get; private set; }

        public ICommand GetPresetQuestionsCommand { get; private set; }


        #endregion

        #region VM Constructors and Methods

        /// <summary>
        /// Constructor, sets up the plant based on IP, Host based address, saves 
        /// a reference to the UI dispatcher and to the current appdata directory
        /// </summary>
        public RackAuditVM()
        {
            //design only data.
            if (IsDesignMode)
            {
                //testing data
                QuestionList.Add(new RackAuditQuestions() { question = "question one goes here", type = 2 });
                QuestionList.Add(new RackAuditQuestions() { question = "question two goes here", type = 1 });
                QuestionList.Add(new RackAuditQuestions() { question = "question three goes here", type = 2 });
                return;
            }
            //grab a dispatcher from the GUI to change it from the VM
            this._dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            //local storage data.
            localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

            //determines plant from IP
            _currentPlant = GetPlantFromIP();
            _apiRoot = GetServiceAddress();
            //client for http operations
            _client.BaseAddress = new Uri(_apiRoot);
            // Add an Accept header for JSON format.
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            QuestionList = new RackAuditQuestionsCollection();
            getQuestionGroups();


            this.NewInspectionCommand = new DelegateCommand(NewInspection);
            this.SearchTicketCommand = new DelegateCommand(getTicketDetails, TicketValidate);
            this.SubmitInspectionCommand = new DelegateCommand(sendAllAnswers, ValidateAnswer);

            this.GetPresetQuestionsCommand = new DelegateCommand(getPresetQuestions);
            /*
             * var dialog = new MessageDialog(message, title);
                await this.OnUiThread(async () => await dialog.ShowAsync());
             * */
        }
        /// <summary>
        /// schedules a task async
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task OnUiThread(Func<Task> action)
        {
            await this._dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => await action());
        }
        #endregion

        #region webApi Methods

        /// <summary>
        /// Gets questionaires based on plant
        /// </summary>
        public async void getQuestionGroups()
        {
            try
            {
                var response = await _client.GetAsync("api/RackAuditQuestionGroup/" + _currentPlant);
                response.EnsureSuccessStatusCode(); // Throw on error code.
                var qs = await response.Content.ReadAsAsync<IEnumerable<RackAuditQuestionGroup>>();
                QuestionGroup = new ObservableCollection<RackAuditQuestionGroup>(qs);

                if (localSettings.Values["selectedQuestionGroup"] != null)
                {
                    int tempCurAudit;
                    int.TryParse(localSettings.Values["selectedQuestionGroup"].ToString(), out tempCurAudit);
                    foreach (RackAuditQuestionGroup gp in QuestionGroup)
                    {
                        if (gp.myKey == tempCurAudit)
                        {
                            CurrentAudit = gp;
                        }
                    }
                    if (CurrentAudit == null && QuestionGroup.Count > 0)
                    {
                        CurrentAudit = QuestionGroup[0];
                    }
                }
                else if (QuestionGroup.Count > 0)
                {
                    CurrentAudit = QuestionGroup[0];
                }

            }
            catch (Exception e)
            {
                Message = "Audit List Failed to load";
            }
        }
        /// <summary>
        /// clears the internal storage for the preset audit set from 
        /// the peek-over top bar
        /// </summary>
        /// <param name="e"></param>
        public void ClearPresetAudit(object e)
        {
            localSettings.Values["selectedQuestionGroup"] = null;
        }

        /// <summary>
        /// This gets called from the scanner and passes a result from the scan.
        /// </summary>
        /// <param name="result">result of the performed scan</param>
        public async void getTicketDetailsFor(Result result)
        {
            if (result != null)
            {
                TicketSerial = result.Text;
                if (result.Text.Substring(0, 1) == "S" && result.Text.Length == TICKETLENGTH)
                {
                    TicketSerial = result.Text.ToLower();
                    string ticketNumber = result.Text.ToLower().TrimStart('s').TrimStart('0');
                    Int64 ticket = Convert.ToInt64(ticketNumber) % TICKETFACTOR;
                    Int64 ticketPlant = Convert.ToInt64(ticketNumber) / TICKETFACTOR;
                    try
                    {
                        var response = await _client.GetAsync("api/FinishedGoods/" + ticketNumber);
                        response.EnsureSuccessStatusCode(); // Throw on error code.
                        var fg = await response.Content.ReadAsAsync<FinishedGoods>();
                        CurrentTicket = fg;
                        getQuestionsFor(ticket, ticketPlant);
                        Message = "Ticket Read Successfully";

                    }
                    catch (Newtonsoft.Json.JsonException jEx)
                    {
                        Message = "Json Problem";

                    }
                    catch (HttpRequestException ex)
                    {
                        Message = "Service not found";
                    }
                    catch (Exception e)
                    {
                        Message = "Ticket Not found";
                    }
                }
                else
                {
                    Message = "Incomplete/unread Ticket";
                }
            }
            else
            {
                Message = "Ticket not read, try to center it";
            }
        }
        /// <summary>
        /// Determines the execution of a the Search Command
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private bool TicketValidate(object parameter)
        {
            if ((TicketSerial == null || TicketSerial.Trim() == "" ||
                                         TicketSerial.ToLower().TrimStart('s').TrimStart('0').Length < 8))
                return false;

            return true;
        }


        /// <summary>
        /// This gets called by manually pressing the Search button
        /// </summary>
        /// <param name="e"></param>
        public async void getTicketDetails(object e)
        {
            string ticketNumber = TicketSerial.ToLower().TrimStart('s').TrimStart('0');
            Int64 ticketN = Convert.ToInt64(ticketNumber) % 10000000;
            Int64 ticketPlant = Convert.ToInt64(ticketNumber) / 10000000;
            try
            {
                var response = await _client.GetAsync("api/FinishedGoods/" + ticketNumber);
                response.EnsureSuccessStatusCode(); // Throw on error code.
                var fg = await response.Content.ReadAsAsync<FinishedGoods>();
                CurrentTicket = fg;
                getQuestionsFor(ticketN, ticketPlant);
                Message = "Ticket Read Successfully";

            }
            catch (Newtonsoft.Json.JsonException jEx)
            {
                Message = "Json Problem";
                TicketFoundException tnf = new TicketFoundException("Json Problem");
            }
            catch (HttpRequestException ex)
            {
                Message = "Service not found";
                TicketFoundException tnf = new TicketFoundException(ex.Message);
            }

        }

        /// <summary>
        /// gets the questions from the API service based on a setup code and a closing code 
        /// </summary>
        /// <param name="setupCode"></param>
        /// <param name="closingCode"></param>
        public async void getQuestionsFor(Int64 ticket, Int64 plant)
        {
            try
            {
                var response = await _client.GetAsync("api/RackAudit/?ticket=" + ticket.ToString() + "&plant=" + plant.ToString() + "&questKey=" + 0);
                response.EnsureSuccessStatusCode(); // Throw on error code
                var list = await response.Content.ReadAsAsync<IEnumerable<RackAuditQuestions>>();
                QuestionList.CopyFrom(list);
            }
            catch (HttpRequestException ex)
            {
                Message = "RackAudit Service Error";
            }
            catch (Exception e)
            {
                Message = "Error Parsing response from server or service not found";
            }

        }

        /// <summary>
        /// reads from the 
        /// </summary>
        public async void getPresetQuestions(object sen)
        {
            try
            {
                var response = await _client.GetAsync("api/RackAudit/?id=" + _presetAuditKey + "&plant=" + _currentPlant);
                response.EnsureSuccessStatusCode(); // Throw on error code.
                var list = await response.Content.ReadAsAsync<IEnumerable<RackAuditQuestions>>();
                QuestionList.CopyFrom(list);
            }
            catch (HttpRequestException ex)
            {
                Message = "RackAudit Service Not found";
            }
            catch (Exception e)
            {
                Message = "Error Parsing response by quest-id from server";
            }
        }

        /// <summary>
        /// Validates the Ticket and Initials to enable Submit button
        /// Warning: This gets called during submit and it might constantly change 
        /// modify VM's message if it gets change inside the validation code.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected bool ValidateAnswer(object parameter)
        {
            if (TicketSerial != null && Initials != null &&
                TicketSerial.Trim().Length >= 8 && Initials.Trim().Length > 0 || (QuestionList.Count > 0 && Initials != null && Initials.Trim().Length > 0))
            {

                return true;
            }
            else
            {

                return false;
            }
        }
        /// <summary>
        /// Automatically determines the plant the surface is connected to 
        /// based on the assigned IP address. It relies on the plant's 
        /// IP scheme
        /// </summary>
        /// <returns></returns>
        public int GetPlantFromIP()
        {

            var icp = NetworkInformation.GetInternetConnectionProfile();
            if (icp != null && icp.NetworkAdapter != null)
            {
                var hostname =
                    NetworkInformation.GetHostNames()
                        .SingleOrDefault(
                            hn =>
                            hn.IPInformation != null && hn.IPInformation.NetworkAdapter != null
                            && hn.IPInformation.NetworkAdapter.NetworkAdapterId
                            == icp.NetworkAdapter.NetworkAdapterId);

                if (hostname != null)
                {
                    int plant;
                    string[] digs = hostname.CanonicalName.Split('.');
                    int.TryParse(digs[1], out plant);
                    return plant;
                }
            }
            //assumes portage when im sitting in my office
            return 12;
        }

        /// <summary>
        /// returns the base WebAPI service address based on the
        /// determined plant, uses localhost if sitting elsewhere
        /// as it assumes debugging.
        /// </summary>
        /// <returns></returns>
        private string GetServiceAddress()
        {
            var hostNames = NetworkInformation.GetHostNames();
            var localName = hostNames.FirstOrDefault(name => name.DisplayName.Contains(".local"));
            var computerName = localName.DisplayName.Replace(".local", "");
            if (computerName.Contains("ptfgssmaguirre"))
            {
                return "http://MRFGSSIISNEW/FGAPI/"; //"http://localhost:63506/"; //
            }

            switch (_currentPlant)
            {
                case 7:
                    return "http://totgssiisnew/FGAPI/";
                case 9:
                    return "http://mnfgssiisnew/FGAPI/";
                case 12:
                    return "http://ptfgssiisnew/FGAPI/";
                case 18:
                    return "http://mrfgssiisnew/FGAPI/";
                case 23:
                    return "http://drfgssiis/FGAPI/";
                case 27:
                    return "http://wlfgssiis/FGAPI/";
                case 30:
                    return "http://chtgssiis/FGAPI/";
                case 32:
                    return "http://mzstssiis/FGAPI/";
                default:
                    return "http://localhost:63506/";

            }

        }

        private void CancelSendingAnswers(object e)
        {
            return;
        }


        public async void sendAllAnswers(object e)
        {
            this.Message = "Sending Audit...";
            submitEnabled = false;
            (SubmitInspectionCommand as DelegateCommand).RaiseCanExecuteChanged();

            RackAuditAnswers answer;
            RackAuditGroup answerGroup = new RackAuditGroup();
            answerGroup.submitPlant = this._currentPlant;
            answerGroup.initials = this.Initials;
            answerGroup.ticket = this.CurrentTicket != null ? this.CurrentTicket.ticket : 0;
            answerGroup.questionGroupID = this._presetAuditKey;
            var client = new HttpClient();
            string picture64 = "";
            bool clear = false;
            bool validation = true;
            foreach (RackAuditQuestions q in QuestionList)
            {
                answer = new RackAuditAnswers(q);
                if (q.type == 2)
                {
                    if (q.pictureStream == null)
                    {
                        Message = "Take the necessary pictures";
                        submitEnabled = true;
                        (SubmitInspectionCommand as DelegateCommand).RaiseCanExecuteChanged();
                        validation = false;
                        break;
                    }
                    var reader = new DataReader(q.pictureStream.GetInputStreamAt(0));
                    var bytes = new byte[q.pictureStream.Size];
                    await reader.LoadAsync((uint)q.pictureStream.Size);
                    reader.ReadBytes(bytes);
                    picture64 = Convert.ToBase64String(bytes);
                    answer.answer = picture64;
                }
                answerGroup.Add(answer);
            }

            if (validation)
            {
                try
                {

                    var response = await client.PostAsJsonAsync<RackAuditGroup>(_apiRoot + "api/rackauditanswers/", answerGroup);
                    response.EnsureSuccessStatusCode();
                    this.Message = "Inspection Sent Correctly";
                    clear = true;
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                }
                //clear the controls and the cached images 
                if (clear)
                {
                    NewInspection(null);
                    StorageFolder temp = ApplicationData.Current.TemporaryFolder;
                    IReadOnlyList<StorageFile> fileList = await temp.GetFilesAsync();
                    foreach (StorageFile f in fileList)
                    {
                        await f.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    }

                }
                submitEnabled = true;
                (SubmitInspectionCommand as DelegateCommand).RaiseCanExecuteChanged();
            }
        }
        #endregion

        /// <summary>
        /// Creates a new inspection, resets the collection of questions
        /// and clears out the fields
        /// </summary>
        /// <param name="e"></param>
        public void NewInspection(object e)
        {
            CurrentTicket = null;
            QuestionList.Reset();
            TicketSerial = "";
            Initials = "";
        }

        /// <summary>
        /// Takes a picture for the question Q
        /// </summary>
        /// <param name="question">a question of the picture type</param>
        async public void TakePictureFor(RackAuditQuestions question)
        {
            // Remember to set permissions in the manifest!
            CameraCaptureUI cameraUI = new CameraCaptureUI();
            cameraUI.PhotoSettings.AllowCropping = false;
            cameraUI.PhotoSettings.MaxResolution = CameraCaptureUIMaxPhotoResolution.MediumXga;
            Windows.Storage.StorageFile capturedMedia = await cameraUI.CaptureFileAsync(CameraCaptureUIMode.Photo);
            if (capturedMedia != null)
            {
                using (var streamCamera = await capturedMedia.OpenAsync(FileAccessMode.Read))
                {
                    BitmapImage bitmapCamera = new BitmapImage();
                    bitmapCamera.SetSource(streamCamera);
                    // This display the image in a bound object to the XAML
                    question.Image = bitmapCamera;
                    int width = bitmapCamera.PixelWidth;
                    int height = bitmapCamera.PixelHeight;
                    question.pictureStream = await capturedMedia.OpenAsync(FileAccessMode.Read);
                    //await capturedMedia.DeleteAsync(StorageDeleteOption.PermanentDelete); 
                }
            }

        }


        //unused
        private static async Task SaveWriteableBitmapAsJpeg(WriteableBitmap bmp, string fileName)
        {
            // Create file in Pictures library and write jpeg to it
            var outputFile = await KnownFolders.PicturesLibrary.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            using (var writeStream = await outputFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                await EncodeWriteableBitmap(bmp, writeStream, BitmapEncoder.JpegEncoderId);
            }

        }

        private static async Task EncodeWriteableBitmap(WriteableBitmap bmp, IRandomAccessStream writeStream, Guid encoderId)
        {
            // Copy buffer to pixels
            byte[] pixels;
            using (var stream = bmp.PixelBuffer.AsStream())
            {
                pixels = new byte[(uint)stream.Length];
                await stream.ReadAsync(pixels, 0, pixels.Length);
            }

            // Encode pixels into stream

            var encoder = await BitmapEncoder.CreateAsync(encoderId, writeStream);
            encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Premultiplied,
               (uint)bmp.PixelWidth, (uint)bmp.PixelHeight,
               96, 96, pixels);
            await encoder.FlushAsync();
        }
    }

}

