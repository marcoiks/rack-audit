﻿using RackAuditMVVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace RackAuditMVVM.Views
{
    /// <summary>
    /// This class is used to determine which Data Template is used based on the
    /// type of the rack audit question.
    /// </summary>
    public class PropertyDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate DataTemplateQuestionYesNo { get; set; }
        public DataTemplate DataTemplateQuestionText { get; set; }
        public DataTemplate DataTemplateQuestionPicture { get; set; }
        public DataTemplate DataTemplateQuestionPassFail { get; set; }
        public DataTemplate DataTemplateQuestion3State { get; set; }
        public DataTemplate DataTemplateQuestionLinkedTicket { get; set; }
        protected override DataTemplate SelectTemplateCore(object item,
                   DependencyObject container)
        {
            RackAuditQuestions dpi = item as RackAuditQuestions;
            if (dpi.type == 0)
            {
                return DataTemplateQuestionYesNo;
            }
            else if (dpi.type == 1)
            {
                return DataTemplateQuestionText;
            }
            else if (dpi.type == 2)
                return DataTemplateQuestionPicture;
            else if (dpi.type == 3)
            {
                return DataTemplateQuestionPassFail;
            }
            else if (dpi.type == 4)
            {
                return DataTemplateQuestionLinkedTicket;
            }
            else
                return DataTemplateQuestionText;
        }
    }
}
