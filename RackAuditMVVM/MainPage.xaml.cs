﻿
using RackAuditMVVM.Models;
using RackAuditMVVM.ViewModel;
using System;
using Windows.Devices.Enumeration;
using Windows.Graphics.Display;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Linq;
using ZXing;

namespace RackAuditMVVM
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        RackAuditVM _vm;

        public MainPage()
        {
            this.InitializeComponent();
            _vm = (RackAuditVM)this.Resources["ViewModel"];
            //TODO: This shouldn't be here, just avoiding the verbose transforms in XAML
            btnScan.Content = "S\nC\nA\nN\n-\nO\nF\nF";
            InitializeCamera();
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            //TODO: This shouldn't be here, just avoiding the verbose transforms in XAML
            btnScan.Content = "S\nC\nA\nN\n-\nO\nN";
            ScanTicket();
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            abortCapture = true;
            //TODO: This shouldn't be here, just avoiding the verbose transforms in 
            btnScan.Content = "S\nC\nA\nN\n-\nO\nF\nF";
        }

        //TODO: This needs to go in the VM to not break MVVM, ZXING keeps trying to access the camera
        /// <summary>
        /// Initializes 
        /// </summary>
        MediaCapture captureManager;
        bool abortCapture;
        bool foundSerialNumber;
        bool secondUse;
        async private void InitializeCamera()
        {
            var cameras = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);
            var rearCamera = cameras.FirstOrDefault(item => item.EnclosureLocation != null && item.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Back);
            var frontCamera = cameras.FirstOrDefault(item => item.EnclosureLocation != null && item.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Front);
            var settings = new MediaCaptureInitializationSettings();
            //settings = new MediaCaptureInitializationSettings { VideoDeviceId = rearCamera != null ? rearCamera.Id : frontCamera.Id };
            settings = new MediaCaptureInitializationSettings();
            if(rearCamera != null)
            {
                settings.VideoDeviceId = rearCamera.Id;
            }
            else if(frontCamera != null)
            {
                settings.VideoDeviceId = frontCamera.Id;
            }
            //old way, now i force it to back camera
            //TODO: investigate  the lack random id for camera array
            //if (cameras.Count == 1)
            //{
            //    settings = new MediaCaptureInitializationSettings { VideoDeviceId = cameras[0].Id }; // 0 => back, 1 => front
            //}
            //else
            //{
            //    settings = new MediaCaptureInitializationSettings { VideoDeviceId = cameras[1].Id };
            //}
            captureManager = new MediaCapture();
            await captureManager.InitializeAsync(settings);
        }

        /// <summary>
        /// Initializes a scan  with the settings of InitializeCamera()
        /// to look for a Ticket serial code 
        /// </summary>
        public async void ScanTicket()
        {
            if (secondUse)
            {
                captureManager = new MediaCapture();
                await captureManager.InitializeAsync(new MediaCaptureInitializationSettings());

            }

            capturePreview.Source = captureManager;
            try
            {
                await captureManager.StartPreviewAsync();
            }
            catch (Exception e)
            {

                throw;
            }
            abortCapture = false;
            foundSerialNumber = false;
            Result result = null;
            while (!abortCapture && !foundSerialNumber)
            {
                var photoStorageFile = await Windows.Storage.KnownFolders.PicturesLibrary.CreateFileAsync("scan.jpg", CreationCollisionOption.GenerateUniqueName);
                await captureManager.CapturePhotoToStorageFileAsync(ImageEncodingProperties.CreateJpeg(), photoStorageFile);

                var stream = await photoStorageFile.OpenReadAsync();
                // initialize with 1,1 to get the current size of the image
                var writeableBmp = new WriteableBitmap(1, 1);
                writeableBmp.SetSource(stream);
                // and create it again because otherwise the WB isn't fully initialized and decoding
                // results in a IndexOutOfRange
                writeableBmp = new WriteableBitmap(writeableBmp.PixelWidth, writeableBmp.PixelHeight);
                stream.Seek(0);
                writeableBmp.SetSource(stream);
                var barcodeReader = new BarcodeReader { AutoRotate = true };
                barcodeReader.Options.TryHarder = true;
                result = barcodeReader.Decode(writeableBmp);
                try
                {
                    _vm.getTicketDetailsFor(result);
                }
                catch (Exception ex)
                {

                }
                if (result != null)
                {
                    foundSerialNumber = true;
                    btnScan.IsChecked = false;

                }
                await photoStorageFile.DeleteAsync(StorageDeleteOption.PermanentDelete);
                //foundSerialNumber = true;

            }
            await captureManager.StopPreviewAsync();
            secondUse = true;
        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            App.Current.Exit();
        }


        private void Button_Click_3(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            _vm.TakePictureFor((sender as Button).DataContext as RackAuditQuestions);
        }

        /// <summary>
        /// This belongs in the UI code as it pertains to logic of the UI only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Create the message dialog and set its content
            var messageDialog = new MessageDialog("Are you sure you want to start a new inspection?");

            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "Yes",
                new UICommandInvokedHandler(_vm.NewInspection)));
            messageDialog.Commands.Add(new UICommand { Label = "No" });

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 1;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;
            // Show the message dialog
            await messageDialog.ShowAsync();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            if (questionsFlip.Items.Count > 0)
            {
                questionsFlip.SelectedIndex = 0;
            }
        }


    }



}
